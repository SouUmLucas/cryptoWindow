﻿using System;
using System.Text;

namespace CryptoWindow
{
    class Validate
    {
        Hash newHash;
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        private string message;
        private string hashedPassword;
        private string hashedConfirmPassword;
        private byte[] EncodedPassword;
        private byte[] EncodedConfirmPassword;
        private byte[] salt;

        public Validate()
        {
            newHash = new Hash();
            salt = newHash.GenerateSalt();
        }

        public string Authenticate()
        {
            if (Password != null && ConfirmPassword != null)
            {
                EncodedPassword = newHash.HashPasswordWithSalt(Encoding.UTF8.GetBytes(Password), salt);
                EncodedConfirmPassword = newHash.HashPasswordWithSalt(Encoding.UTF8.GetBytes(ConfirmPassword), salt);

                hashedPassword = Convert.ToBase64String(EncodedPassword);
                hashedConfirmPassword = Convert.ToBase64String(EncodedConfirmPassword);

                if (hashedPassword == hashedConfirmPassword)
                {
                    message = "Correct Password. Validation completed";
                    return message;
                }
                else
                {
                    message = "Password incorrect!";
                    return message;
                }
            }
            else
            {
                message = "Enter your credentials...";
                return message;
            }
        }
    }
}
